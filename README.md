## Weighted, Blended Order-Independent Transparency with Support for Additive Blending

### 3D Transparent Surface Pass
All geometry is drawn back to front.
FBO layout is the same as the one descibed in [Fast Colored Transparency](http://casual-effects.blogspot.com/2015/03/colored-blended-order-independent.html)

#### Monochrome transmission

##### Blended & Additive
|  Render Target | Format  | Clear     | Src Blend | Dst Blend | Write ("Src")                |
| -------------- | ------- | --------- | --------- | --------- | ---------------------------- |
| accum          | RGBA16F | (0,0,0,0) | ONE       |       ONE | (R.r*a, R.g*a, R.b*a, a) * w |
| revealage      | R8      | (1,0,0,0) | ZERO      | ONE_MINUS_SRC_COLOR | a                  |
| color          | RGB     | -         | ONE       | ONE_MINUS_SRC_ALPHA | a * (A.rgb, 1 - T ) |

#### Colored transmission

In case of colored transmission switching between blended and additive modes requires related switching between different blend functions.

##### Blended
|  Render Target | Format  | Clear     | Src Blend | Dst Blend | Write ("Src")                |
| -------------- | ------- | --------- | --------- | --------- | ---------------------------- |
| accum          | RGBA16F | (0,0,0,0) | ONE       |       ONE | (R.r*a, R.g*a, R.b*a, a) * w |
| revealage      | R8      | (1,0,0,0) | ZERO      | ONE_MINUS_SRC_COLOR | a                  |
| color          | RGB     | -         | ZERO      | ONE_MINUS_SRC_COLOR | a * (1 - T.rgb)    |

##### Additive
|  Render Target | Format  | Clear     | Src Blend | Dst Blend | Write ("Src")                |
| -------------- | ------- | --------- | --------- | --------- | ---------------------------- |
| accum          | RGBA16F | (0,0,0,0) | ZERO      | ONE       | -                            |
| revealage      | R8      | (1,0,0,0) | ZERO      | ONE       | -                            |
| color          | RGB     | -         | ONE       | ONE       | a * A.rgb                    |

### 2D Compositing Pass

The compositing pass is the same as in [Fast Colored Transparency](http://casual-effects.blogspot.com/2015/03/colored-blended-order-independent.html)

#### Shader Code

```
layout (location = 0) out vec4 out_accum;
layout (location = 1) out float out_revealage;
layout (location = 2) out vec4 out_modulate;

void blendedColoredOit(vec4 premultipliedReflect, vec3 transmit) {
    out_modulate = vec4(premultipliedReflect.a * (vec3(1.0) - transmit), 0.0);

    /* Modulate the net coverage for composition by the transmission. This does not affect the color channels of the
       transparent surface because the caller's BSDF model should have already taken into account if transmission modulates
       reflection. See 
       McGuire and Enderton, Colored Stochastic Shadow Maps, ACM I3D, February 2011
       http://graphics.cs.williams.edu/papers/CSSM/
       for a full explanation and derivation.*/

	premultipliedReflect.a *= 1.0 - (transmit.r + transmit.g + transmit.b) * (1.0 / 3.0);
	
	// Intermediate terms to be cubed
    float tmp = (premultipliedReflect.a * 8.0 + 0.01) * (-gl_FragCoord.z * 0.95 + 1.0);
    
    /* If a lot of the scene is close to the far plane, then gl_FragCoord.z does not 
        provide enough discrimination. Add this term to compensate:
        tmp /= sqrt(abs(csZ)); */
	float w    = clamp(tmp * tmp * tmp * 1e3, 1e-2, 3e2);

    out_accum = premultipliedReflect * w;
    out_revealage = premultipliedReflect.a;
}

void blendedMonoOit(vec4 premultipliedReflect, vec3 transmit) {
    premultipliedReflect.a *= 1.0 - (transmit.r + transmit.g + transmit.b) * (1.0 / 3.0);
    out_modulate = vec4(0.0, 0.0, 0.0, premultipliedReflect.a);
    // Intermediate terms to be cubed
    float tmp = (premultipliedReflect.a * 8.0 + 0.01) * (-gl_FragCoord.z * 0.95 + 1.0);
    /* If a lot of the scene is close to the far plane, then gl_FragCoord.z does not 
        provide enough discrimination. Add this term to compensate:
        tmp /= sqrt(abs(csZ)); */
    float w    = clamp(tmp * tmp * tmp * 1e3, 1e-2, 3e2);
    out_accum = premultipliedReflect * w;
    out_revealage = premultipliedReflect.a;
}

void additiveOit(vec3 premultipliedAdditive) {
    out_modulate = vec4(premultipliedAdditive, 0.0);
    out_accum = vec4(0.0);
    out_revealage = 0.0;
}
```